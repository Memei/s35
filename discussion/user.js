const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

mongoose.connect(`mongodb+srv://RMS:Admin123@zuitt-batch197.kblestk.mongodb.net/s35-Activity?retryWrites=true&w=majority`, {

	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection


db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connected to MongoDB!'));

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model('User', userSchema);

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.post('/signup', (req, res) => {
	User.findOne({username: req.body.username},(err, result) =>{
		if(result !==null && result.username === req.body.username){
			return res.send("Duplicate Username Found!")
		} else {
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, savedUser) => {
				console.log(savedUser)

				if(saveErr) {
					return console.err(saveErr)
				} else {
					return res.send("New user registered")
				}

			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port: ${port}`));